import { Link } from '@material-ui/core'
import React from 'react'

import LogoPlaceholder from '../assets/logo-placeholders/DERMOBELLA-HAIR-ONLINE.png'
import Logo from '../assets/logos/DERMOBELLA-HAIR-ONLINE.png'
import { AppCard } from '../components/AppCard'
import { getAppVersions } from '../data/helpers'
import { AppData } from '../data/types'

export function DermoBellaHair(props: AppData) {
  const [androidVersion] = props.apk_url_mng.version.split('_')
  const { apk_version, app_version } = getAppVersions('dermobella-hair')

  return (
    <AppCard
      imageURL={Logo}
      imagePlaceholderURL={LogoPlaceholder}
      licenseOption={props.license.name || 'PROFESSIONAL'}
      androidVersion={apk_version || androidVersion || '4.6.18'}
      iOSVersion={app_version || '1.9.0'}
      androidURL="https://play.google.com/store/apps/details?id=com.chowis.cdp.apptree"
      iOSURL="https://apps.apple.com/us/app/dermobella-hair/id1248460057"
      description={
        <>
          A New professional system for measurement, cosmetic treatment and recommendation of hair
          products. DermoBella Hair is the most advanced range of measuring equipment, which offers
          fast, completely tailored service, catering to each customer's needs. Visit us at
          <Link href="www.chowis.com" target="_blank">
            www.chowis.com
          </Link>
        </>
      }
    />
  )
}
