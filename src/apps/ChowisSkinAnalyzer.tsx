import React from 'react'

import LogoPlaceholder from '../assets/logo-placeholders/SKIN-ANALYZER-ONLINE.png'
import Logo from '../assets/logos/SKIN-ANALYZER-ONLINE.png'
import { AppCard } from '../components/AppCard'
import { AppData } from '../data/types'

export function ChowisSkinAnalyzer(props: AppData) {
  return (
    <AppCard
      imageURL={Logo}
      imagePlaceholderURL={LogoPlaceholder}
      comingSoon
      name="Chowis Skin Analyzer"
      licenseOption=""
      androidVersion=""
      androidURL=""
      iOSURL=""
      description=""
    />
  )
}
