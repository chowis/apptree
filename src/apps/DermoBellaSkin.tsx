import { Link } from '@material-ui/core'
import React from 'react'

import LogoPlaceholder from '../assets/logo-placeholders/DERMOBELLA-SKIN-ONLINE.png'
import Logo from '../assets/logos/DERMOBELLA-SKIN-ONLINE.png'
import { AppCard } from '../components/AppCard'
import { getAppVersions } from '../data/helpers'
import { AppData } from '../data/types'

export function DermoBellaSkin(props: AppData) {
  const { apk_version, app_version } = getAppVersions('dermobella-skin')

  return (
    <AppCard
      imageURL={Logo}
      imagePlaceholderURL={LogoPlaceholder}
      licenseOption={props.license.name || 'STANDARD'}
      androidVersion={apk_version || '1.7.8'}
      iOSVersion={app_version || '1.10.4'}
      androidURL="https://play.google.com/store/apps/details?id=com.dermobellaskincloud.android&hl=en&gl=US"
      iOSURL="https://apps.apple.com/us/app/dermobella-skin/id1153484392"
      description={
        <>
          Skin analysis app is a new professional system for measurement, cosmetic treatment and
          recommendation of skin care products. Skin diagnostic app is the most advanced range if
          measuring equipment, which offers fast, completely tailored service, catering to each
          customer’s needs. Visit us at
          <Link href="www.chowis.com" target="_blank">
            www.chowis.com
          </Link>
        </>
      }
    />
  )
}
