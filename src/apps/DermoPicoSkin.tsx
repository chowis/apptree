import { Link } from '@material-ui/core'
import React from 'react'

import LogoPlaceholder from '../assets/logo-placeholders/DERMOPICO-SKIN-ONLINE.png'
import Logo from '../assets/logos/DERMOPICO-SKIN-ONLINE.png'
import { AppCard } from '../components/AppCard'
import { getAppVersions } from '../data/helpers'
import { AppData } from '../data/types'

export function DermoPicoSkin(props: AppData) {
  const [androidVersion, iosVersion] = props.apk_url_mng.version.split('_')
  const { apk_version, app_version } = getAppVersions('dermopico-skin')

  return (
    <AppCard
      imageURL={Logo}
      imagePlaceholderURL={LogoPlaceholder}
      licenseOption={props.license.name || 'STANDARD'}
      androidVersion={apk_version || androidVersion || '2.2.9'}
      iOSVersion={app_version || iosVersion || '1.8.6'}
      androidURL={
        props.apk_url_mng.apk_url ||
        'https://play.google.com/store/apps/details?id=com.chowis.cma.dermopico'
      }
      iOSURL="https://apps.apple.com/us/app/dermopico-skin/id1378071933"
      description={
        <>
          Mobile Skin Analyzer App: Dermo Pico Skin is a new mobile skin diagnostic app for
          measurement, cosmetic treatment, and recommendation of skin products. DermoPico skin is an
          advanced and mobile diagnostic system which offers fast, completely tailored service,
          catering to each customer's needs. Visit us at{' '}
          <Link href="www.chowis.com" target="_blank">
            www.chowis.com
          </Link>
        </>
      }
    />
  )
}
