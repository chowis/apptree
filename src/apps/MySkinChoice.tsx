import React from 'react'

import LogoPlaceholder from '../assets/logo-placeholders/MYSKIN-CHOICE.png'
import Logo from '../assets/logos/MYSKIN-CHOICE.png'
import { AppCard } from '../components/AppCard'
import { getAppVersions } from '../data/helpers'
import { AppData } from '../data/types'

export function MySkinChoice(props: AppData) {
  const [androidVersion, iosVersion] = props.apk_url_mng.version.split('_')
  const { apk_version, app_version } = getAppVersions('my-skin-choice')

  return (
    <AppCard
      imageURL={Logo}
      imagePlaceholderURL={LogoPlaceholder}
      licenseOption={props.license.name || 'STANDARD'}
      androidVersion={apk_version || androidVersion || '1.9.6'}
      iOSVersion={app_version || iosVersion || '1.2.3'}
      androidURL={
        props.apk_url_mng.apk_url ||
        'https://play.google.com/store/apps/details?id=com.chowis.home.myskin'
      }
      iOSURL="https://apps.apple.com/us/app/myskin-choice/id1500786017"
      description={`The new mySkin Choice is a powerful skin analysis system that's available in your
              smartphone, web browser and tablets. You can analyze your face using our online skin
              analysis (web-based skin analysis) or by using our software application.`}
    />
  )
}
