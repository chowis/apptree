import { Link } from '@material-ui/core'
import React from 'react'

import LogoPlaceholder from '../assets/logo-placeholders/DERMOPICO-HAIR-ONLINE.png'
import Logo from '../assets/logos/DERMOPICO-HAIR-ONLINE.png'
import { AppCard } from '../components/AppCard'
import { getAppVersions } from '../data/helpers'
import { AppData } from '../data/types'

export function DermoPicoHair(props: AppData) {
  const { apk_version, app_version } = getAppVersions('dermopico-hair')

  return (
    <AppCard
      imageURL={Logo}
      imagePlaceholderURL={LogoPlaceholder}
      licenseOption={props.license.name || 'STANDARD'}
      androidVersion={apk_version || '2.3.5'}
      iOSVersion={app_version || '1.4.8'}
      androidURL={
        props.apk_url_mng.apk_url ||
        'https://play.google.com/store/apps/details?id=com.chowis.cma.dermopicohair'
      }
      iOSURL="https://apps.apple.com/us/app/dermopico-hair/id1270671201"
      description={
        <>
          Hair Analyzer App: DermoPico Hair is a new professional system for measuring scalp and
          hair parameters. It features advanced customer registration management, and hair & scalp
          care products and treatments recommendation. DermoPico Hair is an advanced diagnostic app
          which offers fast, completely tailored service, catering to each customer’s needs. Visit
          us at
          <Link href="www.chowis.com" target="_blank">
            www.chowis.com
          </Link>
        </>
      }
    />
  )
}
