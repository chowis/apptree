import 'react-lazy-load-image-component/src/effects/blur.css'

import { ButtonBase, Card, Fade, Grid } from '@material-ui/core'
import { useHover } from 'ahooks'
import React, { useRef } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'
import { Link } from 'react-router-dom'

import p_dpharris from '../assets/device-placeholders/dpharris.jpg'
import p_dpiharris from '../assets/device-placeholders/dpiharris.jpg'
import p_dpiviso from '../assets/device-placeholders/dpiviso.jpg'
import p_dpviso from '../assets/device-placeholders/dpviso.jpg'
import p_dsharris from '../assets/device-placeholders/dsharris.jpg'
import p_dsviso from '../assets/device-placeholders/dsviso.jpg'
import p_miroharris from '../assets/device-placeholders/miroharris.jpg'
import p_miroviso from '../assets/device-placeholders/miroviso.jpg'
import dpharris from '../assets/devices/dpharris.jpg'
import dpiharris from '../assets/devices/dpiharris.jpg'
import dpiviso from '../assets/devices/dpiviso.jpg'
import dpviso from '../assets/devices/dpviso.jpg'
import dsharris from '../assets/devices/dsharris.jpg'
import dsviso from '../assets/devices/dsviso.jpg'
import miroharris from '../assets/devices/miroharris.jpg'
import miroviso from '../assets/devices/miroviso.jpg'
import { Layout } from '../components/Layout'

interface DeviceCardProps {
  imageUrl: string
  path: string
  placeholderSrc: string
}

function DeviceCard({ imageUrl, path, placeholderSrc }: DeviceCardProps) {
  const ref = useRef<HTMLButtonElement>(null)
  const isHovered = useHover(ref)

  return (
    <Fade in>
      <ButtonBase ref={ref}>
        <Link to={path}>
          <Card variant={isHovered ? 'elevation' : 'outlined'} elevation={4}>
            <LazyLoadImage
              alt={imageUrl}
              effect="blur"
              src={imageUrl}
              placeholderSrc={placeholderSrc}
              width="100%"
            />
          </Card>
        </Link>
      </ButtonBase>
    </Fade>
  )
}

export function Home() {
  return (
    <Layout>
      <Grid container direction="column" spacing={4}>
        <Grid item>
          <Grid container spacing={4}>
            <Grid item md={6}>
              <Grid container spacing={4}>
                <Grid item xs={6}>
                  <DeviceCard
                    imageUrl={dpiviso}
                    path="/devices/dpiviso"
                    placeholderSrc={p_dpiviso}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DeviceCard
                    imageUrl={dpiharris}
                    path="/devices/dpiharris"
                    placeholderSrc={p_dpiharris}
                  />
                </Grid>
              </Grid>
            </Grid>

            <Grid item md={6}>
              <Grid container spacing={4}>
                <Grid item xs={6}>
                  <DeviceCard imageUrl={dpviso} path="/devices/dpviso" placeholderSrc={p_dpviso} />
                </Grid>
                <Grid item xs={6}>
                  <DeviceCard
                    imageUrl={dpharris}
                    path="/devices/dpharris"
                    placeholderSrc={p_dpharris}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>

        <Grid item>
          <Grid container spacing={4}>
            <Grid item md={6}>
              <Grid container spacing={4}>
                <Grid item xs={6}>
                  <DeviceCard imageUrl={dsviso} path="/devices/dsviso" placeholderSrc={p_dsviso} />
                </Grid>
                <Grid item xs={6}>
                  <DeviceCard
                    imageUrl={dsharris}
                    path="/devices/dsharris"
                    placeholderSrc={p_dsharris}
                  />
                </Grid>
              </Grid>
            </Grid>
            <Grid item md={6}>
              <Grid container spacing={4}>
                <Grid item xs={6}>
                  <DeviceCard
                    imageUrl={miroviso}
                    path="/devices/miroviso"
                    placeholderSrc={p_miroviso}
                  />
                </Grid>
                <Grid item xs={6}>
                  <DeviceCard
                    imageUrl={miroharris}
                    path="/devices/miroharris"
                    placeholderSrc={p_miroharris}
                  />
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Layout>
  )
}
