import {
  Box,
  Button,
  Card,
  CardContent,
  CircularProgress,
  Grid,
  Link,
  TextField,
  Tooltip,
  Typography,
} from '@material-ui/core'
import { Info } from '@material-ui/icons'
import { Alert } from '@material-ui/lab'
import { useRequest } from 'ahooks'
import React, { useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'
import { useHistory, useParams } from 'react-router-dom'

import { Layout } from '../components/Layout'
import { getDeviceData, getDeviceDataForOldApps } from '../data/deviceAPI'

type DeviceCredentials = {
  optic_number: string
  password: string
}

export function DeviceValidation() {
  const history = useHistory()
  const [error, setError] = useState<string>()
  const { deviceName, opticNumber } = useParams<{ deviceName: string; opticNumber: string }>()
  const isCMADevice = deviceName === 'dpiviso' || deviceName === 'dpiharris'

  const { handleSubmit, register, errors, formState, getValues, reset } = useForm({
    defaultValues: {
      optic_number: '',
      password: '',
    },
  })

  const resultForOldApps = useRequest(getDeviceDataForOldApps, {
    manual: true,
  })

  const validateDeviceRequest = useRequest(getDeviceData, {
    manual: true,
    onSuccess: (response, params) => {
      if (response.data.error) {
        resultForOldApps.run(params[0]).then((data) => {
          if (data.apk) {
            const values = getValues()
            history.push(`/devices/${deviceName}/${values.optic_number}/${values.password}`)
          } else {
            setError(response.data.error)
          }
        })
      } else if (response.data.products) {
        const values = getValues()
        history.push(`/devices/${deviceName}/${values.optic_number}/${values.password}`)
      }
    },
    onError: () => {
      setError('Something went wrong')
    },
  })

  useEffect(() => {
    if (opticNumber) {
      reset({ optic_number: opticNumber, password: '' })
    }
  }, [opticNumber, reset])

  const onSubmit = (values: DeviceCredentials) =>
    validateDeviceRequest.run(values.optic_number, values.password)

  return (
    <Layout>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Box maxWidth="400px" margin="auto">
          <Card variant="outlined">
            <CardContent style={{ paddingRight: 20, paddingLeft: 20 }}>
              <Grid container direction="column" spacing={3}>
                <Grid item>
                  <Grid container alignItems="center">
                    <Grid item xs>
                      <Typography variant="h6" gutterBottom>
                        Device Details
                      </Typography>
                    </Grid>
                    <Grid item>
                      <Tooltip
                        arrow
                        interactive
                        enterTouchDelay={0}
                        title={
                          <Typography>
                            Please contact the distributor or at{' '}
                            <Link href="mailto:help@chowis.com" color="inherit" underline="always">
                              <b>help@chowis.com</b>
                            </Link>{' '}
                            for any questions or support needed.
                          </Typography>
                        }
                      >
                        <Info color="action" />
                      </Tooltip>
                    </Grid>
                  </Grid>
                </Grid>
                {!!error && (
                  <Grid item>
                    <Alert severity="error">{error}</Alert>
                  </Grid>
                )}
                <Grid item>
                  <TextField
                    name="optic_number"
                    inputRef={register({
                      required: isCMADevice ? 'Enter Bluetooth ID' : 'Enter optic number',
                    })}
                    variant="outlined"
                    label={isCMADevice ? 'BT ID (Bluetooth ID)' : 'Optic Number'}
                    size="small"
                    fullWidth
                    error={!!errors.optic_number}
                    helperText={errors.optic_number?.message}
                  />
                </Grid>
                <Grid item>
                  <TextField
                    name="password"
                    inputRef={register({ required: 'Enter password' })}
                    variant="outlined"
                    label="Password"
                    size="small"
                    fullWidth
                    type="password"
                    inputProps={{
                      autocomplete: 'new-password',
                      // form: {
                      //   autocomplete: 'off',
                      // },
                    }}
                    error={!!errors.password}
                    helperText={errors.password?.message}
                  />
                </Grid>
                <Grid item>
                  <Button
                    variant="contained"
                    color="primary"
                    fullWidth
                    disableElevation
                    disabled={!formState.isDirty || formState.isSubmitting}
                    type="submit"
                    endIcon={formState.isSubmitting ? <CircularProgress size={18} /> : undefined}
                  >
                    Submit
                  </Button>
                  <Box paddingY={1}>
                    <Typography align="center" color="textSecondary">
                      OR
                    </Typography>
                  </Box>
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => {
                      history.push(`/devices/${deviceName}/scan_qr`)
                    }}
                    fullWidth
                    disableElevation
                  >
                    Scan QR Code
                  </Button>
                </Grid>
              </Grid>
            </CardContent>
          </Card>
        </Box>
      </form>
    </Layout>
  )
}
