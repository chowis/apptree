/* eslint-disable @typescript-eslint/no-explicit-any */
import { Box } from '@material-ui/core'
import { useRequest } from 'ahooks'
import React, { useEffect, useMemo, useState } from 'react'
import { useParams } from 'react-router-dom'

import { ChowisSkinAnalyzer } from '../apps/ChowisSkinAnalyzer'
import { DermoBellaHair } from '../apps/DermoBellaHair'
import { DermoBellaSkin } from '../apps/DermoBellaSkin'
import { DermoPicoHair } from '../apps/DermoPicoHair'
import { DermoPicoSkin } from '../apps/DermoPicoSkin'
import { MySkinChoice } from '../apps/MySkinChoice'
import { oldApps } from '../apps-old/apps'
import { DermobellasDBS } from '../apps-old/DermobellasDBS'
import { AppCardLoading } from '../components/AppCard'
import { Layout } from '../components/Layout'
import { getDeviceData, getDeviceDataForOldApps } from '../data/deviceAPI'
import { AppData } from '../data/types'

const apps = new Map([
  ['DermoPico Skin', (props: AppData) => <DermoPicoSkin key={props.id} {...props} />],
  ['MySkin Choice', (props: AppData) => <MySkinChoice key={props.id} {...props} />],
  ['DermoPico Hair', (props: AppData) => <DermoPicoHair key={props.id} {...props} />],
  ['DermoBella Hair', (props: AppData) => <DermoBellaHair key={props.id} {...props} />],
  ['DermoBella Skin', (props: AppData) => <DermoBellaSkin key={props.id} {...props} />],
  ['DermoBella Skin 2 Cloud', (props: AppData) => <DermoBellaSkin key={props.id} {...props} />],
  ['Chowis Analyzer Skin', (props: AppData) => <ChowisSkinAnalyzer key={props.id} {...props} />],
])

export function AppDownloadPage() {
  const { deviceName, opticNumber, password } = useParams<{
    deviceName: string
    opticNumber: string
    password: string
  }>()
  const [shits, setShits] = useState<unknown[]>([])

  const result = useRequest(getDeviceData, {
    defaultParams: [opticNumber, password],
  })

  const resultForOldApps = useRequest(getDeviceDataForOldApps, {
    defaultParams: [opticNumber],
  })

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const products = result?.data?.data.products || []

  const filteredProducts = useMemo(() => {
    let next = [...products]

    if (products.find((app: any) => app.apk_url_mng.name === 'DermoBella Skin')) {
      next = products.filter((app: any) => app.apk_url_mng.name !== 'DermoBella Skin 2 Cloud')
    }

    return next
  }, [products])

  const hasChowisSkinAnalyzerDuplicates =
    products?.filter((app: any) => app.apk_url_mng.name.startsWith('Chowis Analyzer Skin'))
      .length === 2

  useEffect(() => {
    if ((deviceName === 'dsviso' || deviceName === 'dpviso') && products.length) {
      setShits((p) => {
        const next = [...p]
        next.push({ apk_url_mng: { name: 'Chowis Analyzer Skin' } })
        return next
      })
    }
  }, [deviceName, products.length])

  const oldApp = resultForOldApps.data?.apk?.apk_url
    ? oldApps[resultForOldApps.data.apk.apk_url] || resultForOldApps.data.apk
    : undefined

  return (
    <Layout>
      <Box>
        {[...filteredProducts, ...shits].map((app: AppData) => {
          const renderAppComponent = apps.get(app.apk_url_mng.name)

          if (
            (deviceName === 'dpiharris' || deviceName === 'miroviso') &&
            app.apk_url_mng.name === 'MySkin Choice'
          ) {
            // shit
            return null
          }

          if (
            hasChowisSkinAnalyzerDuplicates &&
            !!app.apk_url_mng.id &&
            app.apk_url_mng.name === 'Chowis Analyzer Skin'
          ) {
            return null
          }

          return renderAppComponent?.(app)
        })}
        {(result.loading || resultForOldApps.loading) && <AppCardLoading />}

        {!!oldApp && !!resultForOldApps.data?.apk?.apk_url && (
          <DermobellasDBS
            name={oldApp.NAME}
            Logo={oldApp.LOGO}
            version={oldApp.VERSION}
            url={resultForOldApps.data?.apk.apk_url}
          />
        )}
      </Box>
    </Layout>
  )
}
