/* eslint-disable import/no-duplicates */
import { Box, Button, Card, MenuItem, TextField, Typography } from '@material-ui/core'
import { Camera } from '@material-ui/icons'
import React, { useEffect, useRef, useState } from 'react'
import Reader from 'react-qr-scanner'
import QrReader from 'react-qr-scanner'
import { useHistory, useParams } from 'react-router-dom'

import { Layout } from '../components/Layout'

function parseQRCode(value: string): { ON: string; SN: string; SSID: string; BTID: string } {
  return Object.fromEntries(value.split(',').map((f) => f.split(':')))
}

function DetectionArea({ isDetected }: { isDetected?: boolean }) {
  const margin = '50px'
  const size = '50px'
  const borderSize = '10px'
  const borderColor = isDetected ? 'green' : 'red'

  return (
    <>
      <Box
        position="absolute"
        height={size}
        width={size}
        top={margin}
        left={margin}
        borderLeft={`${borderSize} solid ${borderColor}`}
        borderTop={`${borderSize} solid ${borderColor}`}
      />

      <Box
        position="absolute"
        height={size}
        width={size}
        bottom={margin}
        left={margin}
        borderLeft={`${borderSize} solid ${borderColor}`}
        borderBottom={`${borderSize} solid ${borderColor}`}
      />

      <Box
        position="absolute"
        height={size}
        width={size}
        bottom={margin}
        right={margin}
        borderBottom={`${borderSize} solid ${borderColor}`}
        borderRight={`${borderSize} solid ${borderColor}`}
      />

      <Box
        position="absolute"
        height={size}
        width={size}
        top={margin}
        right={margin}
        borderRight={`${borderSize} solid ${borderColor}`}
        borderTop={`${borderSize} solid ${borderColor}`}
      />
    </>
  )
}

export function ScanQRPage() {
  const [shouldUseLegacyMode, setShouldUseLegacyMode] = useState(false)
  const [isDetected, setIsDetected] = useState(false)
  const [selectedDevice, setSelectedDevice] = useState<string>()
  const [devices, setDevices] = useState<MediaDeviceInfo[]>([])
  const { deviceName } = useParams<{ deviceName: string }>()
  const history = useHistory()
  const isCMADevice = deviceName === 'dpiviso' || deviceName === 'dpiharris'

  // const [facingMode, setFacingMode] = useState<'rear' | 'front'>('rear')
  const ref = useRef<Reader>(null)

  useEffect(() => {
    if (navigator) {
      navigator.mediaDevices
        .getUserMedia({ video: true })
        .then(async () => {
          const d = await navigator.mediaDevices.enumerateDevices()
          const videoInputs = d.filter((i) => i.kind === 'videoinput')
          setDevices(videoInputs)
        })
        .catch(() => {
          setShouldUseLegacyMode(true)
        })
    }
  }, [])

  return (
    <Layout>
      <Box maxWidth="400px" margin="auto">
        <Box marginBottom={4}>
          {devices?.length ? (
            <TextField
              value={selectedDevice || devices[0].deviceId}
              label="Select Camera"
              select
              fullWidth
              variant="outlined"
              size="small"
              onChange={(event) => {
                setSelectedDevice(undefined)
                setSelectedDevice(event.target.value)
              }}
              error={shouldUseLegacyMode}
              helperText={
                shouldUseLegacyMode &&
                'Failed to access your camera, click Take Picture button below'
              }
            >
              {devices.map((d) => (
                <MenuItem key={d.deviceId} value={d.deviceId}>
                  {d.label}
                </MenuItem>
              ))}
            </TextField>
          ) : (
            <TextField label="Loading..." disabled fullWidth variant="outlined" size="small" />
          )}
        </Box>

        <Box position="relative" marginBottom={2}>
          <DetectionArea isDetected={isDetected} />
          <Typography color="textSecondary" variant="caption" align="center">
            Place the QR code in front of the camera.
          </Typography>
          <Card variant="outlined">
            <QrReader
              key={selectedDevice}
              ref={ref}
              delay={300}
              style={{
                width: '100%',
                height: window.screen.availWidth < 400 ? 310 : 400,
                objectFit: 'cover',
              }}
              legacyMode={shouldUseLegacyMode}
              onError={(error) => {
                setShouldUseLegacyMode(true)
              }}
              // facingMode="rear"
              onScan={(result) => {
                if (result) {
                  setIsDetected(true)
                  if (isCMADevice) {
                    history.replace(`/devices/${deviceName}/${parseQRCode(result).BTID}`)
                  } else {
                    history.replace(`/devices/${deviceName}/${parseQRCode(result).ON}`)
                  }
                }
              }}
              // eslint-disable-next-line @typescript-eslint/no-explicit-any
              chooseDeviceId={(f) => selectedDevice as any}
            />
          </Card>
        </Box>

        {shouldUseLegacyMode && (
          <Button
            fullWidth
            variant="contained"
            disableElevation
            color="primary"
            startIcon={<Camera />}
            onClick={() => {
              ref.current?.openImageDialog()
            }}
          >
            Take Picture
          </Button>
        )}
      </Box>
    </Layout>
  )
}
