import React from 'react'

import FallbackLogo from '../assets/apk-icon.jpg'
import { OldAppCard } from '../components/OldAppCard'

export function DermobellasDBS({
  Logo,
  name,
  version,
  url,
}: {
  Logo?: string
  name: string
  version: string
  url: string
}) {
  return (
    <OldAppCard
      imageURL={Logo || FallbackLogo}
      imagePlaceholderURL={Logo || FallbackLogo}
      name={name}
      androidVersion={version}
      androidURL={url}
    />
  )
}
