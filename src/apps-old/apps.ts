import Icon3RS from '../assets/old-apps/icon_3r_s.png'
import CellReturn from '../assets/old-apps/icon_cellreturn.png'
import IconH from '../assets/old-apps/icon_h.png'
import IconKLGlobal from '../assets/old-apps/icon_klglobal.png'
import IconKLGlobal2 from '../assets/old-apps/icon_klglobal_2.png'
import IconMAN from '../assets/old-apps/icon_man.png'
import IconS from '../assets/old-apps/icon_s.png'
import IconVagheggi from '../assets/old-apps/icon_vgh.png'
import IconYLJ from '../assets/old-apps/icon_ylj.png'

type OldApp = {
  NAME: string
  VERSION: string
  DATE: string
  URL2: string
  LOGO?: string
}

export const oldApps: Record<string, OldApp> = {
  'http://www.chowis.com/downloads/dp/test/dermoprime.apk': {
    NAME: 'TEST',
    VERSION: '1.0.0',
    DATE: '20181220',
    URL2: 'http://www.chowis.com/downloads/dp/test/dermoprime.apk',
  },
  'http://www.chowis.com/downloads/dp/plus/dermoprime.apk': {
    NAME: 'DERMOPRIME',
    VERSION: '1.8.3p',
    DATE: '20160415',
    URL2: 'http://www.chowis.com/downloads/dp/plus/dermoprime.apk',
  },
  'http://www.chowis.com/downloads/dp/cdp159.apk': {
    NAME: 'DERMOPRIME',
    VERSION: '1.5.9',
    DATE: '20151215',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
  },
  'http://www.chowis.com/downloads/dp/dermoprime.apk': {
    NAME: 'DERMOPRIME',
    VERSION: '2.14.0',
    DATE: '20180719',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
  },
  'http://www.chowis.com/downloads/dbs/dermobellas1.apk': {
    NAME: 'DERMOBELLA SKIN',
    VERSION: '1.6.0',
    DATE: '20170217',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/dbs/dermobellas.apk': {
    NAME: 'DERMOBELLA SKIN',
    VERSION: '3.11.5',
    DATE: '20210305',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/skeyndor2/dermobellas.apk': {
    NAME: 'DERMOBELLA SKIN - Skeyndor',
    VERSION: '3.10.5',
    DATE: '20200601',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/sid/dermobellas.apk': {
    NAME: 'DERMOBELLA SKIN',
    VERSION: '3.9.22',
    DATE: '20200506',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/vagheggi/dermobellas.apk': {
    NAME: 'VGH FACE SKIN ANALYSER',
    VERSION: '3.11.5',
    DATE: '20210305',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconVagheggi,
  },
  'http://www.chowis.com/downloads/3r/dermobellas.apk': {
    NAME: '3R SKIN',
    VERSION: '3.9.4',
    DATE: '20190923',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: Icon3RS,
  },
  'http://www.chowis.com/downloads/amore/dermobellas.apk': {
    NAME: 'DERMOBELLA - AMORE',
    VERSION: '3.4.12',
    DATE: '20180518',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/ovaco/dermobellas.apk': {
    NAME: 'DERMOBELLA - OVACO',
    VERSION: '3.4.8',
    DATE: '20180220',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/kl/dermobellas.apk': {
    NAME: 'DERMOBELLA - SKIN',
    VERSION: '3.4.0',
    DATE: '20180212',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/es/dermobellas.apk': {
    NAME: 'DERMOBELLA - EUNSUNG',
    VERSION: '3.9.17',
    DATE: '20200228',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/skeyndor/dermobellas.apk': {
    NAME: 'DERMOBELLA - SKEYNDOR',
    VERSION: '3.4.15',
    DATE: '20180412',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/dbs/dermobellasx.apk': {
    NAME: 'DERMOBELLA - PMX',
    VERSION: '3.6.4',
    DATE: '20190222',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/kyro/dbsk.apk': {
    NAME: 'DERMOBELLA - KYRO',
    VERSION: '3.9.3',
    DATE: '20190820',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/emb/dermobellas.apk': {
    NAME: 'DERMOBELLA - EMB',
    VERSION: '3.11.5',
    DATE: '20210305',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/qa/dbsproduct2.apk': {
    NAME: 'DERMOBELLA - EMB',
    VERSION: '3.10.4 TEST',
    DATE: '20200526',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/kyro/dbhk.apk': {
    NAME: 'DERMOBELLA - KYRO',
    VERSION: '4.6.7',
    DATE: '20190821',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconH,
  },
  'http://www.chowis.com/downloads/hangisu/dermobellas.apk': {
    NAME: 'DERMOBELLA - HANGISU',
    VERSION: '3.6.7',
    DATE: '20190418',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/ylj/dermobellas.apk': {
    NAME: 'DERMOBELLA - YLJ',
    VERSION: '3.6.7',
    DATE: '20190429',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconYLJ,
  },
  'http://www.chowis.com/downloads/total/dermobellas.apk': {
    NAME: 'DERMOBELLA - TOTAL',
    VERSION: '3.4.0',
    DATE: '20180309',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/db/dermobellah.apk': {
    NAME: 'DERMOBELLA - HAIR',
    VERSION: '4.6.18',
    DATE: '20200723',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconH,
  },
  'http://www.chowis.com/downloads/aveda/dermobellah.apk': {
    NAME: 'DERMOBELLA - HAIR',
    VERSION: '4.3.2',
    DATE: '20180220',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconH,
  },
  'http://www.chowis.com/downloads/meikela/dermobellah.apk': {
    NAME: 'DERMOBELLA - HAIR',
    VERSION: '4.3.12',
    DATE: '20180816',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconH,
  },
  'http://www.chowis.com/downloads/erayba/dermobellah.apk': {
    NAME: 'DERMOBELLA - HAIR',
    VERSION: '4.5.1',
    DATE: '20190430',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconH,
  },
  'http://www.chowis.com/downloads/custom/sobeauty.apk': {
    NAME: 'DERMOBELLA - SKIN',
    VERSION: '3.8.1',
    DATE: '20190626',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/analyzer/siyanliskin.apk': {
    NAME: 'Chowis Analyzer SKIN',
    VERSION: '1.7',
    DATE: '20190904',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/analyzer/siyanlihair.apk': {
    NAME: 'Chowis Analyzer HAIR',
    VERSION: '1.0.0',
    DATE: '20190319',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconH,
  },
  'http://www.chowis.com/downloads/analyzer/chowisskin.apk': {
    NAME: 'Chowis Analyzer SKIN',
    VERSION: '1.7',
    DATE: '20190904',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/analyzer/chowishair.apk': {
    NAME: 'Chowis Analyzer HAIR',
    VERSION: '1.0.0',
    DATE: '20190319',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconH,
  },
  'http://180.166.175.194:5050/shareb/manzhiyans.apk': {
    NAME: 'DERMOBELLA SKIN - KYRO',
    VERSION: '3.8.1',
    DATE: '20190625',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconMAN,
  },
  'http://www.chowis.com/downloads/kyro/samples.apk': {
    NAME: 'DERMOBELLA SKIN - KYRO',
    VERSION: '3.6.7',
    DATE: '20190404',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/kyro/klapp.apk': {
    NAME: 'DERMOBELLA SKIN - KLAPP',
    VERSION: '3.11.1',
    DATE: '20201102',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/humble/dermobellah.apk': {
    NAME: 'DERMOBELLA HAIR',
    VERSION: '4.6.9',
    DATE: '20190911',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconH,
  },
  'http://www.chowis.com/downloads/cell/dermobellas.apk': {
    NAME: 'DERMOBELLA SKIN - CELLRETURN',
    VERSION: '3.10.2',
    DATE: '20200409',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: CellReturn,
  },
  'http://www.chowis.com/downloads/3r/anna.apk': {
    NAME: 'DERMOBELLA SKIN - 3R Anna',
    VERSION: '3.10.9',
    DATE: '20200904',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconS,
  },
  'http://www.chowis.com/downloads/klglobal2/dermobellas.apk': {
    NAME: 'DERMOBELLA SKIN - KL Global',
    VERSION: '3.11.4',
    DATE: '20210226',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconKLGlobal2,
  },
  'http://www.chowis.com/downloads/klglobal/dermobellah.apk': {
    NAME: 'DERMOBELLA HAIR - KL Global',
    VERSION: '4.6.19',
    DATE: '20210226',
    URL2: 'http://www.chowis.com/downloads/dp/cdp159.apk',
    LOGO: IconKLGlobal,
  },
}
