import { Box, Card, CardContent, Fade, Grid, Typography } from '@material-ui/core'
import React from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'

import { AppDownloadButton } from './AppDownloadButton'

interface Props {
  imageURL: string
  imagePlaceholderURL: string
  androidVersion: string
  androidURL: string
  comingSoon?: boolean
  name?: string
}

export function OldAppCard({
  imageURL,
  imagePlaceholderURL,
  androidVersion,
  androidURL,
  comingSoon,
  name,
}: Props) {
  return (
    <Fade in>
      <Box marginBottom={3}>
        <Card variant="outlined">
          <CardContent>
            {!comingSoon && (
              <>
                {' '}
                <Box marginBottom={2} />
                <Grid container alignItems="center" spacing={2}>
                  <Grid item sm={12} md={6} container alignItems="center">
                    <Grid item>
                      <LazyLoadImage
                        effect="blur"
                        src={imageURL}
                        placeholderSrc={imagePlaceholderURL}
                        width="120px"
                      />
                    </Grid>
                    <Grid item>
                      <Typography variant="h5">{name}</Typography>
                      <Typography variant="h6">LATEST ANDROID VERSION: {androidVersion}</Typography>
                    </Grid>
                  </Grid>
                  <Grid item sm={12} md={6}>
                    <Grid container direction="row" spacing={4}>
                      <Grid item xs={12} sm={6}>
                        <AppDownloadButton isOld type="android" url={androidURL} />
                      </Grid>
                    </Grid>
                  </Grid>
                </Grid>
              </>
            )}
          </CardContent>
        </Card>
      </Box>
    </Fade>
  )
}
