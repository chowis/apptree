import { Box, useMediaQuery, useTheme } from '@material-ui/core'
import React from 'react'

import ApptreeLogo from '../assets/logos/APPTREE-LOGO.png'

export function Logo() {
  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.down('sm'))

  return (
    <Box textAlign="center" padding={2} paddingTop={4}>
      <img src={ApptreeLogo} alt="Logo" height={matches ? '80px' : '120px'} />
    </Box>
  )
}
