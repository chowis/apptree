import { Box } from '@material-ui/core'
import React from 'react'

import { Footer } from './Footer'
import { Header } from './Header'
import { Logo } from './Logo'

interface LayoutProps {
  children: React.ReactNode
}

export function Layout(props: LayoutProps) {
  return (
    <div>
      <Header />
      <Logo />

      <Box>{props.children}</Box>
      <Footer />
    </div>
  )
}
