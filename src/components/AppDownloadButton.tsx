import { Box, ButtonBase, Grid, Hidden, Link } from '@material-ui/core'
import { saveAs } from 'file-saver'
import QRCode from 'qrcode.react'
import React from 'react'

import { ReactComponent as AppleStoreBadge } from '../assets/badge_appstore.svg'
import { ReactComponent as GooglePlayStoreBadge } from '../assets/badge_googleplaystore.svg'
import { ReactComponent as Download } from '../assets/download.svg'

export function AppDownloadButton({
  type,
  url,
  isOld,
}: {
  type: 'ios' | 'android'
  url: string
  isOld?: boolean
}) {
  return (
    <Grid container direction="column" spacing={2} alignItems="center">
      <Grid item>
        <Hidden xsDown>
          <QRCode size={180} value={url} />
        </Hidden>
      </Grid>
      <Grid item>
        <Box width="180px">
          {isOld ? (
            <>
              <div>Option 1</div>
              <Link download href={url.replace('http', 'https')}>
                <Download />
              </Link>
              <div>Option 2</div>
              <ButtonBase
                style={{ width: '200px', height: '60px' }}
                onClick={() => {
                  saveAs(url.replace('http', 'https'))
                }}
              >
                <Download />
              </ButtonBase>
            </>
          ) : (
            <Link href={url}>
              {type === 'ios' ? <AppleStoreBadge /> : <GooglePlayStoreBadge />}
            </Link>
          )}
        </Box>
      </Grid>
    </Grid>
  )
}
