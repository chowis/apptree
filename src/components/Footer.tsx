import { Box, Typography, useMediaQuery, useTheme } from '@material-ui/core'
import React from 'react'

import Chowis from '../assets/CHOWIS.png'

export function Footer() {
  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.down('sm'))

  return (
    <Box textAlign="center" paddingTop={4}>
      <Typography color="textSecondary">version: 1.1.5</Typography>
      <img src={Chowis} alt="chowis copy" height={matches ? '25px' : '40px'} />
    </Box>
  )
}
