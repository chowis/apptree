import { Box, Card, CardContent, Fade, Grid, Hidden, Typography } from '@material-ui/core'
import { Skeleton } from '@material-ui/lab'
import React, { ReactNode } from 'react'
import { LazyLoadImage } from 'react-lazy-load-image-component'

import { AppDownloadButton } from './AppDownloadButton'

interface Props {
  imageURL: string
  imagePlaceholderURL: string
  description: ReactNode
  androidVersion: string
  iOSVersion?: string
  licenseOption: string
  androidURL: string
  iOSURL?: string
  comingSoon?: boolean
  name?: string
}

export function AppCard({
  imageURL,
  imagePlaceholderURL,
  description,
  androidVersion,
  iOSVersion,
  licenseOption,
  androidURL,
  iOSURL,
  comingSoon,
  name,
}: Props) {
  return (
    <Fade in>
      <Box marginBottom={3}>
        <Card variant="outlined">
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm="auto">
                <Box textAlign="center">
                  <LazyLoadImage
                    effect="blur"
                    src={imageURL}
                    placeholderSrc={imagePlaceholderURL}
                    width="120px"
                  />
                </Box>
              </Grid>
              <Grid item xs={12} sm>
                {comingSoon ? (
                  <>
                    <Typography variant="h6">{name}</Typography>
                    <Typography variant="h6">COMING SOON</Typography>
                  </>
                ) : (
                  <Typography>{description}</Typography>
                )}
              </Grid>
            </Grid>

            {!comingSoon && (
              <>
                {' '}
                <Box marginBottom={2} />
                <Grid container alignItems="center">
                  <Grid item sm={12} md={6}>
                    <Typography variant="h6">LATEST ANDROID VERSION: {androidVersion}</Typography>
                    {!!iOSVersion && (
                      <Typography variant="h6">LATEST IOS VERSION: {iOSVersion}</Typography>
                    )}
                    <Typography variant="h6">LICENCE OPTION: {licenseOption}</Typography>
                  </Grid>
                  <Grid item sm={12} md={6}>
                    <Grid container direction="row" spacing={4}>
                      <Grid item xs={12} sm={6}>
                        <AppDownloadButton type="android" url={androidURL} />
                      </Grid>

                      {!!iOSURL && (
                        <Grid item xs={12} sm={6}>
                          <AppDownloadButton type="ios" url={iOSURL} />
                        </Grid>
                      )}
                    </Grid>
                  </Grid>
                </Grid>
              </>
            )}
          </CardContent>
        </Card>
      </Box>
    </Fade>
  )
}

export function AppCardLoading() {
  return (
    <Fade in>
      <Box marginBottom={3}>
        <Card variant="outlined">
          <CardContent>
            <Grid container spacing={2}>
              <Grid item xs={12} sm="auto">
                <Box textAlign="center">
                  <Skeleton
                    width="120px"
                    height="120px"
                    variant="rect"
                    style={{ margin: 'auto' }}
                  />
                </Box>
              </Grid>
              <Grid item xs={12} sm>
                <Typography>
                  <Skeleton />
                </Typography>
                <Typography>
                  <Skeleton width="90%" />
                </Typography>
                <Typography>
                  <Skeleton width="80%" />
                </Typography>
              </Grid>
            </Grid>

            <Box marginBottom={2} />

            <Grid container alignItems="center">
              <Grid item sm={12} md={6}>
                <Typography variant="h6">
                  <Skeleton width="300px" />
                </Typography>
                <Typography variant="h6">
                  <Skeleton width="280px" />
                </Typography>
                <Typography variant="h6">
                  <Skeleton width="250px" />
                </Typography>
              </Grid>
              <Grid item sm={12} md={6}>
                <Grid container direction="row" spacing={4}>
                  <Grid item xs={12} sm={6}>
                    <Hidden xsDown>
                      <Skeleton
                        variant="rect"
                        width="180px"
                        height="180px"
                        style={{ margin: 'auto' }}
                      />
                    </Hidden>
                    <Box marginBottom={2} />
                    <Skeleton
                      variant="rect"
                      width="180px"
                      height="53px"
                      style={{ margin: 'auto' }}
                    />
                  </Grid>

                  <Grid item xs={12} sm={6}>
                    <Hidden xsDown>
                      <Skeleton
                        variant="rect"
                        width="180px"
                        height="180px"
                        style={{ margin: 'auto' }}
                      />
                    </Hidden>

                    <Box marginBottom={2} />
                    <Skeleton
                      variant="rect"
                      width="180px"
                      height="53px"
                      style={{ margin: 'auto' }}
                    />
                  </Grid>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </Box>
    </Fade>
  )
}
