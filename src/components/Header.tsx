import { Button, Divider, Fade, Grid, Typography, useMediaQuery, useTheme } from '@material-ui/core'
import { ArrowBack, Home, LocationOn } from '@material-ui/icons'
import { useLocalStorageState, useRequest } from 'ahooks'
import React, { useEffect, useMemo, useState } from 'react'
import Geocode from 'react-geocode'
import useGeolocation from 'react-hook-geolocation'
import { useHistory, useRouteMatch } from 'react-router-dom'

import { IP_LOOKUP_SERVER } from '../data/constants'
import { UserData } from '../data/types'

// set Google Maps Geocoding API for purposes of quota management. Its optional but recommended.
Geocode.setApiKey('AIzaSyDchl7rKeNmvgf10ZXDPicexMwJEfCQeYc')

// set response language. Defaults to english.
Geocode.setLanguage('en')

// Enable or disable logs. Its optional.
// Geocode.enableDebug()

// Get address from latitude & longitude.

export function Header() {
  const theme = useTheme()
  const matches = useMediaQuery(theme.breakpoints.down('sm'))
  const routeMatch = useRouteMatch()
  const history = useHistory()
  const [locationFromGoogle, setLocationFromGoogle] = useState()
  const geolocation = useGeolocation()

  const { data } = useRequest<UserData>(IP_LOOKUP_SERVER)
  const [savedAddress, setSavedAddress] = useLocalStorageState<string>('addr_urs')
  const userLocation = useMemo(() => [data?.city, data?.country].join(', '), [
    data?.city,
    data?.country,
  ])

  useEffect(() => {
    Geocode.fromLatLng(geolocation.latitude, geolocation.longitude).then(
      (response) => {
        const address = response.results[0].formatted_address
        setLocationFromGoogle(address)
      },
      (error) => {
        // eslint-disable-next-line no-console
        console.error(error)
      }
    )
  }, [geolocation.latitude, geolocation.longitude])

  useEffect(() => {
    if (!savedAddress) {
      setSavedAddress(locationFromGoogle)
    }
  }, [locationFromGoogle, savedAddress, setSavedAddress])

  return (
    <div>
      <Grid
        container
        justify="space-between"
        alignItems="center"
        direction={matches ? 'column-reverse' : 'row'}
        spacing={1}
      >
        <Grid item xs>
          <Fade in={routeMatch.path !== '/'} unmountOnExit={matches}>
            <Grid container spacing={2}>
              <Grid item>
                <Button
                  size={matches ? 'large' : 'medium'}
                  startIcon={<ArrowBack />}
                  onClick={() => {
                    history.goBack()
                  }}
                >
                  GO BACK
                </Button>
              </Grid>
              <Grid item>
                <Button
                  size={matches ? 'large' : 'medium'}
                  startIcon={<Home />}
                  onClick={() => {
                    history.push('/')
                  }}
                >
                  HOMEPAGE
                </Button>
              </Grid>
            </Grid>
          </Fade>
        </Grid>

        <Grid item zeroMinWidth>
          <Grid container spacing={1} alignItems="center">
            <Grid item xs>
              <Typography color="textSecondary" display="inline">
                {savedAddress || userLocation}
              </Typography>
            </Grid>
            <Grid item>
              <LocationOn color="action" />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Divider style={{ marginTop: 8 }} />
    </div>
  )
}
