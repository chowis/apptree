import axios from 'axios'

import { BASE_URL } from './Constants'

export const http = axios.create({ baseURL: BASE_URL })

const RAPI_API_KEY = 'aba7e8249amsh0863d0f9a8a2de9p1e4f43jsn219c1e7a30b8'
const RAPI_API_HOST = 'app-stores.p.rapidapi.com'

export function getAndroidAppVersion(appId: string) {
  return axios
    .request({
      method: 'GET',
      url: 'https://app-stores.p.rapidapi.com/details',
      params: { store: 'google', id: appId, language: 'en' },
      headers: {
        'x-rapidapi-key': RAPI_API_KEY,
        'x-rapidapi-host': RAPI_API_HOST,
      },
    })
    .then((response) => response.data.currentVersion)
}

export function getIOSAppVersion(appId: string) {
  return axios
    .request({
      method: 'GET',
      url: 'https://app-stores.p.rapidapi.com/details',
      params: { store: 'apple', id: appId, language: 'en' },
      headers: {
        'x-rapidapi-key': RAPI_API_KEY,
        'x-rapidapi-host': RAPI_API_HOST,
      },
    })
    .then((response) => response.data.currentVersion)
}
