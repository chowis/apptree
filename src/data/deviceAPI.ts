import axios from 'axios'

import { http } from '../api/API'
import { OldAppData } from './types'

export function getDeviceData(optic_number: string, password: string) {
  return http.get(`/product/info?optic_number=${optic_number}&password=${password}`)
}

export async function getDeviceDataForOldApps(optic_number: string) {
  await axios.post(
    `https://app.chowis.com/apk/apkUseReset.do?optic_number=${optic_number}&mac_address=kkkkk&admin=Y`
  )

  const res = await axios.post(
    `https://app.chowis.com/apk/apkInfo.do?optic_number=${optic_number}&use_date=20201010&use_time=121212&mac_address=kkkkk`
  )

  return res.data as { apk: OldAppData | undefined }
}
