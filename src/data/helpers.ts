export function getAppVersions(name: string): { apk_version: string; app_version: string } {
  const data = window.localStorage.getItem(name)
  return data ? JSON.parse(data) : {}
}
