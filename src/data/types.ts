export type UserData = {
  businessName: string
  businessWebsite: string
  city: string
  continent: string
  country: string
  countryCode: string
  ipName: string
  ipType: string
  isp: string
  lat: string
  lon: string
  org: string
  query: string
  region: string
  status: string
}

export type AppData = {
  apk: {
    id: number
    optic_number: string
    serial_number: string
    docking_number: string
    wb: boolean
  }
  apk_url_mng: {
    id: number
    name: string
    apk_url: string
    version: string
  }
  app_use_yn: string
  created_at: string
  first_use_date: string
  id: number
  license: { id: number; name: string }
  license_period: number
  mac_address: string
  use_date: string
  use_time: string
}

export type OldAppData = {
  apk_url: string
  division: string
  id: number
  optic_number: string
  regist_date: number
  update_date: unknown
  use_yn: string
  version: string
}
