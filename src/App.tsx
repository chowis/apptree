import { Box } from '@material-ui/core'
import { AnimatePresence, motion } from 'framer-motion'
import React, { ReactNode } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import { getAndroidAppVersion, getIOSAppVersion } from './api/API'
import { AppDownloadPage } from './pages/AppDownloadPage'
import { DeviceValidation } from './pages/DeviceValidation'
import { Home } from './pages/HomePage'
import { ScanQRPage } from './pages/ScanQRPage'

function FadeIn({ children }: { children: ReactNode }) {
  return (
    <motion.div initial={{ opacity: 0 }} animate={{ opacity: 1 }} exit={{ opacity: 0 }}>
      {children}
    </motion.div>
  )
}

const APPS = [
  { name: 'dermobella-hair', apk_id: 'com.chowis.cdp.apptree', app_id: '1248460057' },
  { name: 'dermobella-skin', apk_id: 'com.dermobellaskincloud.android', app_id: '1153484392' },
  { name: 'dermopico-hair', apk_id: 'com.chowis.cma.dermopicohair', app_id: '1270671201' },
  { name: 'dermopico-skin', apk_id: 'com.chowis.cma.dermopico', app_id: '1378071933' },
  { name: 'my-skin-choice', apk_id: 'com.chowis.home.myskin', app_id: '1500786017' },
]

APPS.forEach((app) => {
  Promise.all([getAndroidAppVersion(app.apk_id), getIOSAppVersion(app.app_id)]).then(
    ([apk_version, app_version]) => {
      window.localStorage.setItem(app.name, JSON.stringify({ apk_version, app_version }))
    }
  )
})

function App() {
  return (
    <Box maxWidth="1280px" margin="auto" paddingX={2}>
      <div style={{ position: 'relative' }}>
        <AnimatePresence key={window.location.pathname}>
          <BrowserRouter>
            <Switch>
              <Route path="/devices/:deviceName/scan_qr">
                <FadeIn>
                  <ScanQRPage />
                </FadeIn>
              </Route>
              <Route path="/devices/:deviceName/:opticNumber/:password">
                <FadeIn>
                  <AppDownloadPage />
                </FadeIn>
              </Route>
              <Route path="/devices/:deviceName/:opticNumber?">
                <FadeIn>
                  <DeviceValidation />
                </FadeIn>
              </Route>
              <Route path="/">
                <motion.div
                  key="2"
                  initial={{ opacity: 0, y: 100 }}
                  animate={{ opacity: 1, y: 0 }}
                  exit={{ opacity: 0, y: 50 }}
                >
                  <Home />
                </motion.div>
              </Route>
            </Switch>
          </BrowserRouter>
        </AnimatePresence>
      </div>
    </Box>
  )
}

export default App
